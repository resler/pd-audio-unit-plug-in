/*
 BiQuad2.cpp
 Pd++
 
 Created by Esler,Robert Wadhams on 9/26/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 
 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
 */

#ifndef __Pd____BiQuad2__
#define __Pd____BiQuad2__

#include <string>
#include "PdMaster.h"
#include <string>

namespace pd {

/*! \brief A two-pole, two-zero filter.
        
 Biquad takes a list of five parameters. The parameters in order are:
 feedback1, feedback2, feedforward1, feedforward2, feedforward3.
     
*/
    
class BiQuad : public PdMaster {
    
private:
    
    typedef struct biquadctl
    {
        float c_x1=0;
        float c_x2=0;
        float c_fb1=0;
        float c_fb2=0;
        float c_ff1=0;
        float c_ff2=0;
        float c_ff3=0;
    } t_biquadctl;
    
    typedef struct sigbiquad
    {
        
        float x_f;
        t_biquadctl x_cspace;
        t_biquadctl *x_ctl;
    } t_sigbiquad;
    
    /*prototypes and globals*/
    t_sigbiquad *x = new t_sigbiquad;
    int globalCounter = 0;
    float last = 0;
    float prev = 0;
    
    /*Used by setCoefficients()*/
    void list(double fb1, double fb2, double ff1, double ff2, double ff3);
    
public:
    BiQuad();
    ~BiQuad();
    double perform(double );
    void setCoefficients(double fb1, double fb2, double ff1, double ff2, double ff3);
    void set(double a, double b);
    
    const std::string pdName = "biquad~";
};

} // pd namespace
#endif /* defined(__Pd____BiQuad2__) */
