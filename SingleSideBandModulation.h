/*
 SingleSideBandModulation.cpp
 Pd++
 
 Created by Robert Esler on 9/23/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
*/

#ifndef __Pd____SingleSideBandModulation__
#define __Pd____SingleSideBandModulation__

#include <string>
#include <iostream>
#include <math.h>
#include "BiQuad.h"
#include "SineWave.h"
#include "CosineWave.h"
#include "PdMaster.h"

namespace rwe {

/*! \brief Single Sideband Modulation.
     
     This class is based directly the hilbert~ and complex-mod~
     abstractions in Pure Data.  It uses a hilbert filter by cascading
     a series of biquad filters, which offsets the phase of the signal by 180
     degrees. Then the real and imaginary parts are modulation using a cosine and
     sine wave respectively. The input is your signal to be modulated.
     
*/
    
class SSB : public PdMaster {
    
public:
    SSB();
    ~SSB();
    void setOscFreq(double input);
    double getOscFreq();
    double perform(double input1);
    
private:
    pd::BiQuad biquad[4];
    rwe::SineWave sine;
    rwe::CosineWave cosine;
    std::string name = "SSB";
    double phasorFreq = 100;
    
    /*Produce our Hilbert filter, outputs should be around 90 deg out of phase.*/
    double leftPhase(double input);
    double rightPhase(double input);
    
    
};

} // rwe namespace
#endif /* defined(__Pd____SingleSideBandModulation__) */
