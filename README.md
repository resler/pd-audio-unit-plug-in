# README #

### What is this project about? ###

* Audio Unit is an Apple audio plug-in format.
* Audio Units can be used in popular DAW's for audio processing and as synthesizers
* Pd++ is a C++ library based on Pure Data 
* [See Pd++ repository](https://bitbucket.org/resler/pd)
* You can develop audio units using Pd++, this project is an example

### How do I use this project? ###

* Download Pure Data if you have not already [Pd](http://www.puredata.info)
* Download or clone the [Pd++](https://bitbucket.org/resler/pd) library and get familiar with the syntax, object model and classes.  The classes are all based on Pure Data's Vanilla objects.
* Download or clone this project and study how I used my Pd++ algorithm
* Create a new Xcode project with the same settings, files, and formats
* Build your project, fix errors, and now you have your own AU

### Notes ###

* You will need to download the Audio Unit SDK from Apple [AU SDK](https://developer.apple.com/library/mac/documentation/MusicAudio/Conceptual/AudioUnitProgrammingGuide/Introduction/Introduction.html)
* Import all of the required files or provide Xcode with the path to the headers to Apple's  AUPublic and PublicUtility folders.
* Import the proper frameworks for AU, for this project they are
        * CoreAudio
        * CoreServices
        * AudioToolbox
        * AudioUnit
* Make sure to change your .plist, .exp, .pch and *Version.h files to fit your plug-in
* This project has two .exp and a .r file that were for a 32-bit build.  If you only plan to use 64-bit plug-ins then you can remove the AUssb.exp and AUssb.r files.
   
   I have only tested this plug-in using a 64-bit build.  I have tested it in Logic Pro X, GarageBand, and AULab and validated it using auval.
   Good luck with building for 32-bit.  It seems Apple no longer supports the development side of that very well.  64-bit works fine however.  

### License ###

Copyright (c) 2015 by Robert Esler
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.