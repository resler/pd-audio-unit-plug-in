#include <AudioUnit/AudioUnit.r>
#include "ssbUnitVersion.h"
// Note that resource IDs must be spaced 2 apart for the 'STR ' name and description
#define kAudioUnitResID_ssbUnit 1000

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SSB ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#define RES_ID          kAudioUnitResID_ssbUnit
#define COMP_TYPE       kAudioUnitType_Effect
#define COMP_SUBTYPE    ssbUnit_COMP_SUBTYPE
#define COMP_MANUF      ssbUnit_COMP_MANF
#define VERSION         kSsbUnitVersion
#define NAME            "Troo: Single Side-Band Modulation AU"
#define DESCRIPTION     "SSBAU"
#define ENTRY_POINT     "ssbUnitEntry"

#include "AUResources.r"