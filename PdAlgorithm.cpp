/* This class is where you put all your audio routines, etc.
  PdAlgorithm.cpp
  Pd++

  This class emulates a Pd Patch.  Your algorithm or patch code would 
  be simulated in the runAlgorithm(double, double) function.  
  Subpatches and abstractions could be member functions.
  Abstractions and externals could be separate classes.
  The runAlgorithm receives audio input and writes the output
  to the Buffer data type.  Buffer is defined in the superclass PdMaster.
  The audio I/O class paRender only calls the runAlgorithm function, so
  all your audio output data must end up in this function via objects
  or member functions.

  NOTE: It's best not to write to the stdout (e.g std::cout, or printf()) in
  your runAlgorithm function, unless you are using mainNoAudio.cpp. It could
  cause audio interruptions.
 
  Created by Robert Esler on 9/17/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/

#include "PdAlgorithm.h"

PdAlgorithm::PdAlgorithm() {
   
    /*Initialize stuff here*/
   

}

PdAlgorithm::~PdAlgorithm() {
    
}

/*! This is where the actual algorithm gets called by paRender
 you can use other non-static functions of your class but 
 paRender only cares about the Buffer that is returned from
 this function.  Put any functions here that need to be updated.
 Be wary of using locking or wait routines, allocating or deallocating memory. */

Buffer PdAlgorithm::runAlgorithm(double input1, double input2) {
    
    Buffer theBuffer;
    
    /*This is the audio input stream*/
    theBuffer.inbuf1 = input1;
    theBuffer.inbuf2 = input2;
    
    /*put your audio objects here, initialize them in the constructor.
      Use the destructor to dealloc and write out buffers, etc.*/
    double output1;
   
    ssb1.setOscFreq(freqShift);
    output1 = ssb1.perform(input1);
    
       /*write output to buf.outbufL, buf.outbufR*/
    theBuffer.outbufL =  output1;
    
    
     return theBuffer;
    
}

void PdAlgorithm::setFrequencyShift(double fs) {
    
    freqShift = fs;
    
}

