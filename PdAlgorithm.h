/*! \brief This class is where you put all your audio routines, etc.
 
 This class emulates a Pd Patch.  Your algorithm or patch code would
 be simulated in the runAlgorithm(double, double) function.
 Subpatches and abstractions could be member functions.
 Abstractions and externals could be separate classes.
 The runAlgorithm receives audio input and writes the output
 to the Buffer data type.  Buffer is defined in the superclass PdMaster.
 The audio I/O class paRender only calls the runAlgorithm function, so
 all your audio output data must end up in this function via objects
 or member functions.
 
 NOTE: It's best not to write to the stdout (e.g std::cout, or printf()) in
 your runAlgorithm function, unless you are using mainNoAudio.cpp. It could
 cause audio interruptions.
 
 Created by Robert Esler on 9/17/14.
 Copyright (c) 2014 Robert Esler. All rights reserved.
 */

#ifndef __Pd____PdAlgorithm__
#define __Pd____PdAlgorithm__

#include "PdMaster.h"
#include "SingleSideBandModulation.h"

/*A simulation of a Pd Patch, this is where your audio code will be placed.*/
class PdAlgorithm : public PdMaster {
    
    /*Pd++ objects created here, plus other private members*/
private:
    
    rwe::SSB ssb1;
    double freqShift = 0;
    
public:
    
    PdAlgorithm();
    ~PdAlgorithm();
    Buffer runAlgorithm(double input1, double input2);
    void setFrequencyShift(double fs);
    double getFrequencyShift() {return freqShift;};
};

#endif /* defined(__Pd____PdAlgorithm__) */
