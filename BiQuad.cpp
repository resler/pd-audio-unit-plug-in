/*
  BiQuad2.cpp
  Pd++

  Created by Esler,Robert Wadhams on 9/26/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
 
 Some of the code in Pd++ is Copyright (c) 1997-1999 Miller Puckette.
 For information on usage and redistribution, and for a DISCLAIMER OF ALL
 WARRANTIES, see the file, "LICENSE.txt," in this distribution.
 */

#include "BiQuad.h"

namespace pd {

BiQuad::BiQuad() {
    x->x_ctl = &x->x_cspace;
    x->x_cspace.c_x1 = x->x_cspace.c_x2 = 0;
   
    x->x_f = 0;
}

BiQuad::~BiQuad() {
    
    delete x;
}

void BiQuad::setCoefficients(double fb1, double fb2, double ff1, double ff2, double ff3) {
    
    x->x_ctl->c_fb1 = fb1;
    x->x_ctl->c_fb2 = fb2;
    x->x_ctl->c_ff1 = ff1;
    x->x_ctl->c_ff2 = ff2;
    x->x_ctl->c_ff3 = ff3;
    
    BiQuad::list(x->x_ctl->c_fb1, x->x_ctl->c_fb2, x->x_ctl->c_ff1, x->x_ctl->c_ff2, x->x_ctl->c_ff3);
    
}

double BiQuad::perform(double input) {
    
    float output = 0;
    t_biquadctl *c = x->x_ctl;
    
//    float last = c->c_x1;
//    float prev = c->c_x2;
    float fb1 = c->c_fb1;
    float fb2 = c->c_fb2;
    float ff1 = c->c_ff1;
    float ff2 = c->c_ff2;
    float ff3 = c->c_ff3;
    
   
    
        float fbSum =  input + fb1 * last + fb2 * prev;
        if (PD_BIGORSMALL(fbSum))
            fbSum = 0;
        output = ff1 * fbSum + ff2 * last + ff3 * prev;
        prev = last;
        last = fbSum;
        

//    c->c_x1 = last;
//    c->c_x2 = prev;
    return output;
}
    

void BiQuad::list(double fb1, double fb2, double ff1, double ff2, double ff3){
    
    float discriminant = fb1 * fb1 + 4 * fb2;
    t_biquadctl *c = x->x_ctl;
    if (discriminant < 0) /* imaginary roots -- resonant filter */
    {
        /* they're conjugates so we just check that the product
         is less than one */
        if (fb2 >= -1.0f) goto stable;
    }
    else    /* real roots */
    {
        /* check that the parabola 1 - fb1 x - fb2 x^2 has a
         vertex between -1 and 1, and that it's nonnegative
         at both ends, which implies both roots are in [1-,1]. */
        if (fb1 <= 2.0f && fb1 >= -2.0f &&
            1.0f - fb1 -fb2 >= 0 && 1.0f + fb1 - fb2 >= 0)
            goto stable;
    }
    /* if unstable, just bash to zero */
    fb1 = fb2 = ff1 = ff2 = ff3 = 0;
stable:
    c->c_fb1 = fb1;
    c->c_fb2 = fb2;
    c->c_ff1 = ff1;
    c->c_ff2 = ff2;
    c->c_ff3 = ff3;
}


void BiQuad::set(double a, double b) {
    t_biquadctl *c = x->x_ctl;
    c->c_x1 = a;
    c->c_x2 = b;
    
}
    
} // pd namespace
