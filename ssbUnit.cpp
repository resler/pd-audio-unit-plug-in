//
//  ssbUnit.cpp
//  AUssb
//
//  Created by Robert Esler on 2/11/15.
//  Copyright (c) 2015 Robert Esler. All rights reserved.
//

#include "ssbUnit.h"

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AUDIOCOMPONENT_ENTRY(AUBaseFactory, ssbUnit)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// The COMPONENT_ENTRY macro is required for the Mac OS X Component Manager to recognize and
// use the audio unit



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	ssbUnit::ssbUnit
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// The constructor for new TremoloUnit audio units
ssbUnit::ssbUnit (AudioUnit component) : AUEffectBase (component) {
    
    // This method, defined in the AUBase superclass, ensures that the required audio unit
    //  elements are created and initialized.
    CreateElements ();
    
    // Invokes the use of an STL vector for parameter access.
    //  See AUBase/AUScopeElement.cpp
    Globals () -> UseIndexedParameters (kNumberOfParameters);
    
    // During instantiation, sets up the parameters according to their defaults.
    //	The parameter defaults should correspond to the settings for the default preset.
    SetParameter (
                  kParameter_Frequency,
                  kDefaultValue_Modulation_Freq
                  );
    
    
    // Also during instantiation, sets the preset menu to indicate the default preset,
    //	which corresponds to the default parameters. It's possible to set this so a
    //	fresh audio unit indicates the wrong preset, so be careful to get it right.
    SetAFactoryPresetAsCurrent (
                                kPresets [kPreset_Default]
                                );
    
#if AU_DEBUG_DISPATCHER
    mDebugDispatcher = new AUDebugDispatcher (this);
#endif
}

#pragma mark ____Parameters

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	ssbUnit::GetParameterInfo
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Called by the audio unit's view; provides information needed for the view to display the
//  audio unit's parameters
ComponentResult ssbUnit::GetParameterInfo (
                                               AudioUnitScope			inScope,
                                               AudioUnitParameterID	inParameterID,
                                               AudioUnitParameterInfo	&outParameterInfo
                                               ) {
    ComponentResult result = noErr;
    
    // Adds two flags to all parameters for the audio unit, indicating to the host application
    // that it should consider all the audio unit’s parameters to be readable and writable.
    outParameterInfo.flags =
    kAudioUnitParameterFlag_IsWritable | kAudioUnitParameterFlag_IsReadable;
    
    // All three parameters for this audio unit are in the "global" scope.
    if (inScope == kAudioUnitScope_Global) {
        switch (inParameterID) {
                
            case kParameter_Frequency:
                // Invoked when the view needs information for the frequency
                // parameter; defines how to represent this parameter in the user interface.
                AUBase::FillInParameterName (
                                             outParameterInfo,
                                             kParamName_Modulation_Freq,
                                             false
                                             );
                
                outParameterInfo.unit			= kAudioUnitParameterUnit_Hertz;
                // Sets the unit of measurement for the Frequency parameter to Hertz.
                outParameterInfo.minValue		= kMinimumValue_Modulation_Freq;
                // Sets the minimum value for the Frequency parameter.
                outParameterInfo.maxValue		= kMaximumValue_Modulation_Freq;
                // Sets the maximum value for the Frequency parameter.
                outParameterInfo.defaultValue	= kDefaultValue_Modulation_Freq;
                // Sets the default value for the Frequency parameter.
                //outParameterInfo.flags			|= kAudioUnitParameterFlag_DisplayLogarithmic;
                // Adds a flag to indicate to the host that it should use a logarithmic
                // control for the Frequency parameter.
                break;
                
                           
            default:
                result = kAudioUnitErr_InvalidParameter;
                break;
        }
    } else {
        result = kAudioUnitErr_InvalidParameter;
    }
    return result;
}



#pragma mark ____Properties

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	TremoloUnit::GetPropertyInfo
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ComponentResult ssbUnit::GetPropertyInfo (
                                              // This audio unit doesn't define any custom properties, so it uses this generic code for
                                              // this method.
                                              AudioUnitPropertyID	inID,
                                              AudioUnitScope		inScope,
                                              AudioUnitElement	inElement,
                                              UInt32				&outDataSize,
                                              Boolean				&outWritable
                                              ) {
    return AUEffectBase::GetPropertyInfo (inID, inScope, inElement, outDataSize, outWritable);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	TremoloUnit::GetProperty
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ComponentResult ssbUnit::GetProperty (
                                          // This audio unit doesn't define any custom properties, so it uses this generic code for
                                          // this method.
                                          AudioUnitPropertyID inID,
                                          AudioUnitScope 		inScope,
                                          AudioUnitElement 	inElement,
                                          void				*outData
                                          ) {
    return AUEffectBase::GetProperty (inID, inScope, inElement, outData);
}

#pragma mark ____Factory Presets

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	TremoloUnit::GetPresets
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// For users to be able to use the factory presets you define, you must add a generic
// implementation of the GetPresets method. The code here works for any audio unit that can
// support factory presets.

// The GetPresets method accepts a single parameter, a pointer to a CFArrayRef object. This
// object holds the factory presets array generated by this method. The array contains just
// factory preset numbers and names. The host application uses this array to set up its
// factory presets menu and when calling the NewFactoryPresetSet method.

ComponentResult ssbUnit::GetPresets (
                                         CFArrayRef	*outData
                                         ) const {
    
    // Checks whether factory presets are implemented for this audio unit.
    if (outData == NULL) return noErr;
    
    // Instantiates a mutable Core Foundation array to hold the factory presets.
    CFMutableArrayRef presetsArray = CFArrayCreateMutable (
                                                           NULL,
                                                           kNumberPresets,
                                                           NULL
                                                           );
    
    // Fills the factory presets array with values from the definitions in the TremoloUnit.h
    // file.
    for (int i = 0; i < kNumberPresets; ++i) {
        CFArrayAppendValue (
                            presetsArray,
                            &kPresets [i]
                            );
    }
    
    // Stores the factory presets array at the outData location.
    *outData = (CFArrayRef) presetsArray;
    return noErr;
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	TremoloUnit::NewFactoryPresetSet
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// The NewFactoryPresetSet method defines all the factory presets for an audio unit. Basically,
// for each preset, it invokes a series of SetParameter calls.

// This method takes a single argument of type AUPreset, a structure containing a factory
//  preset name and number.
OSStatus ssbUnit::NewFactoryPresetSet (
                                           const AUPreset &inNewFactoryPreset
                                           ) {
    // Gets the number of the desired factory preset.
    SInt32 chosenPreset = inNewFactoryPreset.presetNumber;
    
    if (
        // Tests whether the desired factory preset is defined.
        chosenPreset == kPreset_Choppy ||
        chosenPreset == kPreset_Crazy  ||
        chosenPreset == kPreset_Hyper
        ) {
        // This 'for' loop, and the 'if' statement that follows it, allow for noncontiguous preset
        // numbers.
        for (int i = 0; i < kNumberPresets; ++i) {
            if (chosenPreset == kPresets[i].presetNumber) {
                
                //Selects the appropriate case statement based on the factory preset number.
                switch (chosenPreset) {
                        
                        // The settings for the "Choppy" factory preset.
                    case kPreset_Choppy:
                        SetParameter (
                                      kParameter_Frequency,
                                       kParameter_Preset_Modulation_Frequency_Choppy
                                      );
                      
                        break;
                        
                        // The settings for the "Crazy" factory preset.
                    case kPreset_Crazy:
                        SetParameter (
                                      kParameter_Frequency,
                                       kParameter_Preset_Modulation_Frequency_Crazy
                                      );
                        
                        break;
                        
                        // The settings for the "Hyper" factory preset.
                    case kPreset_Hyper:
                        SetParameter (
                                      kParameter_Frequency,
                                      kParameter_Preset_Modulation_Frequency_Hyper
                                      );
    
                        break;
                }
                
                // Updates the preset menu in the generic view to display the new factory preset.
                SetAFactoryPresetAsCurrent (
                                            kPresets [i]
                                            );
                return noErr;
            }
        }
    }
    return kAudioUnitErr_InvalidProperty;
}

#pragma mark ____ssbUnitEffectKernel

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	ssbUnit::ssbUnitKernel::ssbUnitKernel()
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// This is the constructor for the ssbnitKernel helper class, which holds the DSP code
//  for the audio unit. TremoloUnit is an n-to-n audio unit; one kernel object gets built for
//  each channel in the audio unit.
//
// The first line of the method consists of the constructor method declarator and constructor-
//  initializer. In addition to calling the appropriate superclasses, this code initializes two
//  member variables:
//
// (In the Xcode template, the header file contains the call to the superclass constructor.)
ssbUnit::ssbUnitKernel::ssbUnitKernel (AUEffectBase *inAudioUnit ) : AUKernelBase (inAudioUnit)
{
    
    //audio code here
    
    sampleRate = inAudioUnit->GetSampleRate();
    pd.setSampleRate(static_cast<double>(sampleRate));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//	TremoloUnit::TremoloUnitKernel::Reset()
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Because we're calculating each output sample based on a unique input sample, there's no
// need to clear any buffers. We simply reinitialize the variables that were initialized on
// instantiation of the kernel object.
void ssbUnit::ssbUnitKernel::Reset() {

}

// This method contains the DSP code.
void ssbUnit::ssbUnitKernel::Process (
                                              const Float32 *inSourceP,	// The audio sample input buffer.
                                              Float32 *inDestP,	// The audio sample output buffer.
                                              UInt32 inSamplesToProcess, // The number of samples in the input buffer.
                                              UInt32 inNumChannels,	// The number of input channels. This is always equal to 1
                                              //   because there is always one kernel object instantiated
                                              //   per channel of audio.
                                              bool	&ioSilence	// A Boolean flag indicating whether the input to the audio
//   unit consists of silence, with a TRUE value indicating
//   silence.
) {
    // Ignores the request to perform the Process method if the input to the audio unit is silence.
    if (!ioSilence) {
        
        // Assigns a pointer variable to the start of the audio sample input buffer.
        const Float32 *sourceP = inSourceP;
        
        // Assigns a pointer variable to the start of the audio sample output buffer.
        Float32	*destP = inDestP,
        inputSample,			// The current audio sample to process.
        outputSample,			// The current audio output sample resulting from one iteration of the
        //   processing loop.
        modulationFrequency;	// The modulation frequency requested by the user via the audio unit's view.
     
       //Get the parameter
        modulationFrequency = GetParameter (kParameter_Frequency);
        // Performs bounds checking on the parameters.
        if ( modulationFrequency	< kMinimumValue_Modulation_Freq)
             modulationFrequency	= kMinimumValue_Modulation_Freq;
        if ( modulationFrequency	> kMaximumValue_Modulation_Freq)
             modulationFrequency	= kMaximumValue_Modulation_Freq;
        
        //set the mod freq in the pd algorithm
        pd.setFrequencyShift(static_cast<double>(modulationFrequency));
        
        // The sample processing loop: processes the current batch of samples, one sample at a time.
        for (int i = inSamplesToProcess; i > 0; --i) {
            
            // more audio code here
            
            // Gets the next input sample.
            inputSample			= *sourceP;
            
            Buffer pdOutput = pd.runAlgorithm(inputSample, 0);
            // Calculates the next output sample.
            outputSample		= pdOutput.outbufL;
            
            // Stores the output sample in the output buffer.
            *destP				= outputSample;
            
            // Advances to the next sample location in the input and output buffers.
            sourceP				+= 1;
            destP				+= 1;
            
           
        }
    }
}

