//
//  ssbUnit.h
//  AUssb
//
//  Created by Robert Esler on 2/11/15.
//  Copyright (c) 2015 Robert Esler. All rights reserved.
//


#include "AUEffectBase.h"
#include "ssbUnitVersion.h"
#include "PdAlgorithm.h"

#if AU_DEBUG_DISPATCHER
#include "AUDebugDispatcher.h"
#endif


#ifndef __AUssb__ssbUnit__
#define __AUssb__ssbUnit__

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Constants for parameters and  factory presets
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#pragma mark __AUssb__ssbUnit__ Parameter Constants

// Provides the user interface name for the Frequency parameter.
static CFStringRef kParamName_Modulation_Freq		= CFSTR ("Frequency");
// Defines a constant for the default value for the Frequency parameter, anticipating a
//  unit of Hertz to be defined in the implementation file.
static const float kDefaultValue_Modulation_Freq	= 0.0;
// Defines a constant for the minimum value for the Frequency parameter.
static const float kMinimumValue_Modulation_Freq	= -400.0;
// Defines a constant for the maximum value for the Frequency parameter.
static const float kMaximumValue_Modulation_Freq	= 400.0;

// Defines constants for identifying the parameters; defines the total number
//  of parameters.
enum {
    kParameter_Frequency	= 0,
    kNumberOfParameters		= 1
};

#pragma mark __AUssb__ssbUnit__ Factory Preset Constants

// Defines a constant for the frequency value for the "Choppy"
static const float kParameter_Preset_Modulation_Frequency_Choppy	= 12.0;

// Defines a constant for the frequency value for the "Crazy"
static const float kParameter_Preset_Modulation_Frequency_Crazy 	= -300.0;

static const float kParameter_Preset_Modulation_Frequency_Hyper     = 300.0;


enum {
    // Defines a constant for the "Choppy" factory preset.
    kPreset_Choppy	= 0,
    
    // Defines a constant for the "Crazy" factory preset.
    kPreset_Crazy	= 1,
    
    kPreset_Hyper   = 2,
    
    // Defines a constant representing the total number of factory presets.
    kNumberPresets	= 3
};

// Defines an array containing two Core Foundation string objects. The objects contain
//  values for the menu items in the user interface corresponding to the factory presets.
static AUPreset kPresets [kNumberPresets] = {
    {kPreset_Choppy, CFSTR ("Choppy")},
    {kPreset_Crazy, CFSTR ("Crazy")},
    {kPreset_Hyper, CFSTR ("Hyper")}
};

// Defines a constant representing the default factory preset, in this case the
//  "Crazy" preset.
static const int kPreset_Default = kPreset_Crazy;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// TremoloUnit class
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#pragma mark __AUssb__ssbUnit__
class ssbUnit : public AUEffectBase {
    
public:
    ssbUnit (AudioUnit component);
    
#if AU_DEBUG_DISPATCHER
    virtual ~ssbUnit () {delete mDebugDispatcher;}
#endif
    
    virtual AUKernelBase *NewKernel () {return new ssbUnitKernel(this);}
    
    
    virtual	ComponentResult GetParameterInfo (
                                              AudioUnitScope			inScope,
                                              AudioUnitParameterID	inParameterID,
                                              AudioUnitParameterInfo	&outParameterInfo
                                              );
    
    virtual ComponentResult GetPropertyInfo (
                                             AudioUnitPropertyID		inID,
                                             AudioUnitScope			inScope,
                                             AudioUnitElement		inElement,
                                             UInt32					&outDataSize,
                                             Boolean					&outWritable
                                             );
    
    virtual ComponentResult GetProperty (
                                         AudioUnitPropertyID		inID,
                                         AudioUnitScope			inScope,
                                         AudioUnitElement		inElement,
                                         void					*outData
                                         );
    
    // report that the audio unit supports the
    //	kAudioUnitProperty_TailTime property
    virtual	bool SupportsTail () {return true;}
    
    // provide the audio unit version information
    virtual ComponentResult	Version () {return kSsbUnitVersion;}
    
    // Declaration for the GetPresets method (for setting up the factory presets),
    //  overriding the method from the AUBase superclass.
    virtual ComponentResult	GetPresets (
                                        CFArrayRef	*outData
                                        ) const;
    
    // Declaration for the NewFactoryPresetSet method (for setting a factory preset
    //  when requested by the host application), overriding the method from the
    //  AUBase superclass.
    virtual OSStatus NewFactoryPresetSet (
                                          const AUPreset	&inNewFactoryPreset
                                          );
    
protected:
    class ssbUnitKernel : public AUKernelBase {
    public:
        ssbUnitKernel (AUEffectBase *inAudioUnit);
        
        // *Required* overides for the process method for this effect
        // processes one channel of interleaved samples
        virtual void Process (
                              const Float32 	*inSourceP,
                              Float32		 	*inDestP,
                              UInt32 			inFramesToProcess,
                              UInt32			inNumChannels, // equal to 1
                              bool			&ioSilence
                              );
        
        virtual void Reset ();
        
    private:
        Float32 sampleRate;
        PdAlgorithm pd; //runs the algorithm from Pd++
        enum	{sampleLimit = (int) 10E6};	// To keep the value of mSamplesProcessed within a 
        //   reasonable limit. 10E6 is equivalent to the number   
        //   of samples in 100 seconds of 96 kHz audio.
        
    };
};




#endif /* defined(__AUssb__ssbUnit__) */
