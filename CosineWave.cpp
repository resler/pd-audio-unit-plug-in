/*
  CosineWave.cpp
  Pd++

  Created by Esler,Robert Wadhams on 9/24/14.
  Copyright (c) 2014 Robert Esler. All rights reserved.
*/

/* A cosine wave generator.
 
 This generator uses the Pd++ class phasor to generate its
 ramp.
 
 */
#include "CosineWave.h"
#include "math.h"

namespace rwe {

CosineWave::CosineWave() {
    
    
}

CosineWave::~CosineWave() {
    
}

/*Every Pd++ class should have a perform() function that outputs a double
 The input should simulate the inlets of the Pd object.
 If your object outputs more than one data stream, use a typedef struct*/
double CosineWave::perform(double input) {
    
    double output=0;
    CosineWave::setFrequency(input);
//    samplesPerCycle = CosineWave::getSampleRate()/CosineWave::getFrequency();
//    factor = (2*3.14159265358979)/samplesPerCycle;
    volume = CosineWave::getVolume();
//    output = (volume * cos(iterator++*factor));
   output = (volume * cos(2 * 3.14159265358979 * phasor.perform(CosineWave::getFrequency())));
    return output;
    
}

void CosineWave::setFrequency(double freq) {
    
    frequency = freq;
}

/*! Volume range is from 0-1. */
void CosineWave::setVolume(double v) {
    
    if (v > 1)
        v = 1;
    if (v < 0)
        v = 0;
    
    volume = v;
}

} //rwe namespace
