//
//  ssbUnitVersion.h
//  AUssb
//
//  Created by Robert Esler on 2/11/15.
//  Copyright (c) 2015 Robert Esler. All rights reserved.
//

#ifndef AUssb_ssbUnitVersion_h
#define AUssb_ssbUnitVersion_h


#ifdef DEBUG
#define kSsbUnitVersion 0xFFFFFFFF
#else
#define kSsbUnitVersion 0x00010000
#endif

// customized for each audio unit
#define ssbUnit_COMP_SUBTYPE		'Plug'
#define ssbUnit_COMP_MANF			'Troo'

#endif
